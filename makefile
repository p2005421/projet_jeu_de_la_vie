CORE_SRC = core/cellule.cpp core/grille.cpp core/perso.cpp core/jeu.cpp core/chat.cpp #core/main.cpp


TXT_SRC = $(CORE_SRC) txt/txtJeu.cpp txt/winTxt.cpp txt/main_TXT.cpp
TARGET_TXT = m


SDL_SRC = $(CORE_SRC) sdl2/main_sdl.cpp sdl2/sdlJeu.cpp
TARGET_SDL = sd

CC = g++ -g -Wall

BIN = bin
SRC = src
OBJ = obj
DATA = data
INCLUDE_DIR	= -Isrc -Isrc/core -Isrc/sdl2 -Itxt

ifeq ($(OS),Windows_NT)
	INCLUDE_DIR_SDL = 	-Iextern/SDL2_mingw-cb20/SDL2-2.0.12/x86_64-w64-mingw32/include/SDL2 \
						-Iextern/SDL2_mingw-cb20/SDL2_ttf-2.0.15/x86_64-w64-mingw32/include/SDL2 \
						-Iextern/SDL2_mingw-cb20/SDL2_image-2.0.5/x86_64-w64-mingw32/include/SDL2 \
						-Iextern/SDL2_mingw-cb20/SDL2_mixer-2.0.4/x86_64-w64-mingw32/include/SDL2

	LIBS_SDL = -Lextern \
			-Lextern/SDL2_mingw-cb20/SDL2-2.0.12/x86_64-w64-mingw32/lib \
			-Lextern/SDL2_mingw-cb20/SDL2_ttf-2.0.15/x86_64-w64-mingw32/lib \
			-Lextern/SDL2_mingw-cb20/SDL2_image-2.0.5/x86_64-w64-mingw32/lib \
			-Lextern/SDL2_mingw-cb20/SDL2_mixer-2.0.4/x86_64-w64-mingw32/lib \
			-lmingw32 -lSDL2main -lSDL2.dll -lSDL2_ttf.dll -lSDL2_image.dll -lSDL2_mixer.dll

else
	INCLUDE_DIR_SDL = -I/usr/include/SDL2
	LIBS_SDL = -lSDL2 -lSDL2_ttf -lSDL2_image -lSDL2_mixer
endif

all: sdl txt


make_dir:
ifeq ($(OS),Windows_NT)
	if not exist $(OBJ) mkdir $(OBJ) $(OBJ)\core $(OBJ)\txt $(OBJ)\sdl2
else
	test -d $(OBJ) || mkdir -p $(OBJ) $(OBJ)/core $(OBJ)/txt $(OBJ)/sdl2
endif


txt : make_dir $(BIN)/$(TARGET_TXT)
sdl : make_dir $(BIN)/$(TARGET_SDL)


$(BIN)/$(TARGET_TXT): $(TXT_SRC:%.cpp=$(OBJ)/%.o)
	$(CC) $+ -o $@


$(BIN)/$(TARGET_SDL): $(SDL_SRC:%.cpp=$(OBJ)/%.o)
	$(CC) $+ -o $@ $(LIBS_SDL)


$(OBJ)/%.o: $(SRC)/%.cpp
	$(CC) -c $(INCLUDE_DIR_SDL) $(INCLUDE_DIR)  $< -o $@


doc: doc/sims.doxy
	cd doc; doxygen sims.doxy

clean:
ifeq ($(OS),Windows_NT)
	del /f $(OBJ) $(BIN_DIR)\$(TARGET_TXT).exe $(BIN)/$(TARGET_SDL).exe
else
	rm -rf $(OBJ) $(BIN)/$(TARGET_TXT) $(BIN)/$(TARGET_SDL) doc/html doc/latex
endif
