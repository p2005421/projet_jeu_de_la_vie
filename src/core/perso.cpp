#include <iostream>
#include "perso.h"

std::string regarde[4]= {"haut","bas","gauche","droite"};

Personnage::Personnage() {

    pos.assign(2,0);
    faim = 50;
    soif = 50;
    sommeil = 50;
    etatMental = 50;
    hygiene = 50;
    sante = 50;
    dir.assign(2,0);
    dir[0]=0;
    dir[1]=1;
    countblesse=1;
    direction=regarde[0];
    malade = false;
    
}

Personnage::~Personnage() {
    faim = 0;
    soif = 0;
    sommeil = 0;
    etatMental = 0;
    hygiene = 0;
    sante = 0;
    countblesse=1;
}

unsigned int Personnage::getSommeil() const {
    return sommeil;
}

unsigned int Personnage::getFaim() const {
    return faim;
}

unsigned int Personnage::getSoif() const {
    return soif;
}

unsigned int Personnage::getMental() const {
    return etatMental;
}

unsigned int Personnage::getHygiene() const {
    return hygiene;
}

unsigned int Personnage::getSante() const {
    return sante;
}

unsigned int Personnage::getBlesse() const {
    return countblesse;
}

bool Personnage::getMalade() const {
    return malade;
}


void Personnage::setSommeil(int valSommeil) {
    sommeil = valSommeil;
}

void Personnage::setFaim(int valFaim) {
    faim = valFaim;
}

void Personnage::setSoif(int valSoif) {
    soif = valSoif;
}

void Personnage::setMental(int valMental) {
    etatMental = valMental;
}

void Personnage::setHygiene(int valHyg) {
    hygiene = valHyg;
}
    
void Personnage::setSante(int valSante) {
    sante = valSante;
}

void Personnage::setBlesse(int valcountblesse) {
    countblesse = valcountblesse;
}

void Personnage::setMalade(bool test) {
    malade = test;
}


void Personnage::droite(const Grille & g) {
    if (g.estValide(pos[0]+1,pos[1])) {
        pos[0]++;
    }
    else (countblesse++);
    dir[0]=pos[0]+1;
    dir[1]=pos[1];
    direction=regarde[3];
}

void Personnage::gauche(const Grille & g) {
    if (g.estValide(pos[0]-1,pos[1])) {
        pos[0]--;
    }
    else (countblesse++);
    dir[0]=pos[0]-1;
    dir[1]=pos[1];
    direction=regarde[2];
}

void Personnage::haut(const Grille & g) {
    if (g.estValide(pos[0],pos[1]+1)) {
        pos[1]++;
    }
    else (countblesse++);
    dir[1]=pos[1]+1;
    dir[0]=pos[0];
    direction=regarde[0];
}

void Personnage::bas(const Grille &  g) {
    if (g.estValide(pos[0],pos[1]-1)==1) {
        pos[1]--;
    }
    else (countblesse++);
    dir[1]=pos[1]-1;
    dir[0]=pos[0];
    direction=regarde[1];
}


void Personnage::mvtAuto(const Grille& g) {
    int roll;
    do {
        roll = rand()%4;
        if (roll == 0) {
            gauche(g);
        } else if (roll == 1) {
            droite(g);
        } else if (roll == 2) {
            haut(g);
        } else if (roll == 3) {
            bas(g);
        }
    } while (!g.estValide(pos[0],pos[1]));

    std::cout << "Activation mouvement auto." << std::endl;
}


void Personnage::afficherPos() {

    std::cout << std::endl;
    std::cout << "faim : " << faim << std::endl;
    std::cout << "soif : " << soif << std::endl;
    std::cout << "sommeil : " << sommeil << std::endl;
    std::cout << "mental : " << etatMental << std::endl;
    std::cout << "hygiène : " << hygiene << std::endl;
    std::cout << "santé : " << sante << std::endl;
    //std::cout <<" dir :"  << dir[0]<<"  "<< dir[1]<< std::endl;
    int a,b;
    a=getX();
    b=getY();
    std::cout <<" position :"  << a<<"  "<< b<< std::endl;

}


int Personnage::getX() const {
    return pos[0];
}

int Personnage::getY() const {
    return pos[1];
}

int Personnage::getDirX() const {
    return dir[0];
}

int Personnage::getDirY() const {
    return dir[1];
}

void Personnage::testRegressionPerso() {

    Personnage a;
    std::vector<int> t_pos, t_dir;
    t_pos.assign(2,0);
    t_dir.assign(2,0);
    Grille g;
    unsigned int t_faim=0, t_soif=0, t_sommeil=0, t_etatMental=0,t_hygiene=0, t_sante=0, t_countblesse;
    bool t_malade;

    assert(a.faim==50);
    assert(a.soif==50);
    assert(a.sommeil==50);
    assert(a.etatMental==50);
    assert(a.hygiene==50);
    assert(a.sante==50);
    assert(a.getX()==0);
    assert(a.getY()==0);
    assert(a.getDirX()==0);
    assert(a.getDirY()==1);

    t_pos[0] = a.getX();
    t_pos[1] = a.getY();

    t_dir[0] = a.getDirX();
    t_dir[1] = a.getDirY();

    t_faim = a.getFaim();
    t_soif = a.getSoif();
    t_sommeil = a.getSommeil();
    t_etatMental = a.getMental();
    t_hygiene = a.getHygiene();
    t_sante = a.getSante();
    t_countblesse = a.getBlesse();
    t_malade = a.getMalade();

    assert (t_faim == a.faim && t_soif == a.soif && t_sommeil == a.sommeil && t_countblesse == a.countblesse);
    assert (t_etatMental == a.etatMental && t_hygiene == a.hygiene && t_sante == a.sante && t_malade == a.malade);
    assert (t_pos[0]==0 && t_pos[1]==0 && t_dir[0]==0 && t_dir[1]==1);

    a.setFaim(46);
    assert (a.faim == 46);
    
    a.setSoif(89);
    assert (a.soif == 89);

    a.setSommeil(36);
    assert (a.sommeil == 36);

    a.setMental(75);
    assert(a.etatMental == 75);

    a.setHygiene(95);
    assert(a.hygiene == 95);

    a.setSante(99);
    assert (a.sante==99);

    a.setBlesse(3);
    assert (a.countblesse==3);

    a.setMalade(true);
    assert (a.malade == true);

    // Position (x,y), on passe de (0,0) à (0,1)
    // donc le vecteur direction est de (0,2) en theorie
    a.haut(g);
    assert(a.getY()== t_pos[1]+1 && a.getDirY() == t_dir[1]+1);

    // Position (x,y), on passe de (0,1) à (0,0)
    // donc le vecteur direction est de (0,-1) en theorie
    a.bas(g);
    assert(a.getY()== t_pos[1] && a.getDirY() == t_dir[1]-2);

    // Position (x,y), on passe de (0,0) à (1,0)
    // donc le vecteur direction est de (2,0) en theorie
    a.droite(g);
    assert(a.getX()== t_pos[0]+1 && a.getDirX() == t_dir[0]+2);

    // Position (x,y), on passe de (1,0) à (0,0)
    // donc le vecteur direction est de (-1,0) en theorie
    a.gauche(g);
    assert(a.getX()== t_pos[0] && a.getDirX()== t_dir[0]-1);
}

