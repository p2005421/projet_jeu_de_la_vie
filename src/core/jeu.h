/**
 * @brief Module permettant de générer toutes les fonctions liées au jeu
 * @file /core/jeu.h
 */

#ifndef _JEU
#define _JEU

#include "cellule.h"
#include "grille.h"
#include "perso.h"
#include "chat.h"

/**
 * @brief La classe "Jeu", qui crée un terrain et un personnage
*/
class Jeu
{
    public :
    Grille terrain;
    Personnage perso;
    Chat chat;
    /**
     * @brief Constructeur du jeu, il initialise une grille et un personnage,
     * tout en réalisant les tests de régression nécéssaires
    */
    Jeu ();

    /**
     * @brief Permet au joueur de controler le personnage
     * @param touche Touche associée à une action (se déplacer, interagir...)
    */
    void EtatSuivCtrl(const char touche);

    /**
     * @brief En appuyant sur la touche associée,
     * cela permet au personnage de faire des mouvements automatiques.
     * @param touche Touche associée à l'activation de la fonction
    */
    void EtatSuivAuto(const char touche);

    /**
     * @brief Fonction qui permet au joueur de travailler s'il est proche d'une cellule le permettant (un bureau),
     * réduisant ainsi sa santé mentale
    */
    void Travail();

    /**
     * @brief Permet au joueur de manger s'il est proche d'une cellule qui le permet,
     * cela augmente sa valeur en faim, sauf s'il est déjà à 100
    */
    void Mange();

    /**
     * @brief Permet au joueur de dormir s'il est proche d'une cellule qui le permet,
     * cela augmente sa jauge de sommeil, sauf s'il est déjà à 100 
    */
    void Dormir();

    /**
     * @brief Permet au joueur de se doucher s'il est proche d'une cellule qui le permet,
     * cela augmente l'hygiène du personnage
    */
    void seDoucher();

    /**
     * @brief Permet au joueur de se doucher s'il est proche d'une cellule qui le permet,
     * tout comme la fonction "seDoucher()", cela permet d'augmenter l'hygiène du personnage
    */
    void allerToilettes();

    /**
     * @brief Fonction qui donne des probabilités au personnage d'attraper une maladie,
     * ce qui réduit sa santé plus vite s'il en attrape et qu'il ne se soigne pas
     * @param temps Récupère le temps de lancement du programme
     * @param maladietps Donne un intervalle de temps ou l'on lance un dé entre 1 et 100
     * @param vitesse Vitesse d'écoulement de la jauge de santé
    */
    void Maladie(int temps, int maladietps, int vitesse);

    /**
     * @brief Fonction qui permet au joueur de construire la case qu'il veut,
     * et aux coordonnées qu'il veut
    */
    void Construire();

    /**
     * @brief Fonction qui blesse le personnage s'il se cogne 3 fois contre un mur,
     * réduisant ainsi sa santé de moitié
    */
    void seBlesse();

    /**
     * @brief Fonction qui donne des mouvement aléatoires au personnage,
     * si sa santé mentale atteint 0, il est possible d'en sortir s'il se divertit
    */
    void Demence(int temps, int vitesse);

    /**
     * @brief Récupère l'état du personnage, s'il est vivant ou mort.
     * Le personnage meurt si sa santé atteint 0
     * @returns un booléen
    */ 
    bool Mort();

    /**
     * @brief Lance le processus de fin du jeu lorsque le personnage est mort
    */
    void FinJeu();
    /**
    * @brief Déclenche le processus de fin du jeu lorsque le perso est mort 
    */

    /**
     * @brief Permet au joueur de soigner son personnage s'il est proche d'une cellule qui le permet,
     * augmente la santé du personnage de 5
    */
    void seSoigner();

    /**
     * @brief Permet au personnage de boire s'il est proche d'une cellule le permettant,
     * augmentant ainsi la jauge de soif du personnage
    */
    void Boire();

    /**
     * @brief Permet au personnage de se divertir s'il est proche d'une cellule qui le permet,
     * augmente la santé mentale du personnage et l'empêche de devenir fou
    */
    void seDivertir();

    /**
     * @brief Permet au personnage de se divertir s'il est proche d'une cellule qui le permet,
     * augmente la santé mentale du personnage et l'empêche de devenir fou
    */
    void Biblio();

    /**
     * @brief Permet au joueur de se laver les mains s'il est proche d'une cellule qui le permet,
     * cela augmente l'hygiène du personnage
    */
    void LaverMain();

    /**
     * @brief Le personnage caresse son chat, ce qui augmente sa santé mentale,
     * et l'empêche de devenir fou 
    */
    void CaresseChat();

    /**
     * @brief Récupère le terrain dans une constante
     * @returns la Grille sur laquelle le jeu se déroule
    */
    const Grille& getGrille() const;

    /**
     * @brief Récupère le personnage dans une constante
     * @returns le personnage que le joueur contrôle
    */
    const Personnage& getPerso() const;
    
    void ConstruireDim(int x,int x1,int y,int y1,char c);

    const Grille& getConstTerrain () const;
    const Personnage& getConstPerso () const;

    /**
    * @brief Ecoulement des variables : 
    *  @param vitesse = Vitesse d'écoulement
    */
    
    /**
     * @brief Permet l'écoulement de la jauge de Sommeil au cours du temps,
     * @param temps Temps récupéré par le programme
     * @param vitesse Vitesse à laquelle la jauge s'écoule
    */
    void EcouleSommeil(int temps, int vitesse);

    /**
     * @brief Permet l'écoulement de la jauge de Soif au cours du temps,
     * @param temps Temps récupéré par le programme
     * @param vitesse Vitesse à laquelle la jauge s'écoule
    */
    void EcouleSoif(int temps, int vitesse);

    /**
     * @brief Permet l'écoulement de la jauge de Faim au cours du temps,
     * @param temps Temps récupéré par le programme
     * @param vitesse Vitesse à laquelle la jauge s'écoule
    */
    void EcouleFaim(int temps, int vitesse);

    /**
     * @brief Permet l'écoulement de la jauge de santé mentale au cours du temps,
     * @param temps Temps récupéré par le programme
     * @param vitesse Vitesse à laquelle la jauge s'écoule
    */
    void EcouleMental(int temps, int vitesse);

    /**
     * @brief Permet l'écoulement de la jauge d'hygiène au cours du temps,
     * @param temps Temps récupéré par le programme
     * @param vitesse Vitesse à laquelle la jauge s'écoule
    */
    void EcouleHygiene(int temps, int vitesse);

    /**
     * @brief Permet l'écoulement de la jauge de Santé au cours du temps,
     * la vie baisse si une des autres variables (faim, soif...) est inférieure à 25
     * @param temps Temps récupéré par le programme
     * @param vitesse Vitesse à laquelle la jauge s'écoule
    */
    void EcouleSante(int temps, int vitesse); 

    /**
     * @brief Déplace le chat sur le terrain de manière aléatoire
     * @param temps Temps récupéré par le programme
     * @param vitesse Vitesse à laquelle la jauge s'écoule
    */
    void DeplacerChat(int temps, int vitesse);

    /**
     * @brief Récupère le chat dans une constante
     * @returns le Chat qui se déplace sur le terrain
    */
    const Chat& getChat() const;

    /**
     * @brief Génère de la nourriture de manière aléatoire sur le terrain,
     * quand le personnage mange la nourriture
    */
    void NourritureApp();

    /**
     * @brief Génère une cellule de soin de manière aléatoire sur le terrain,
     * quand le personnage se soigne
    */
    void SoinApp();

    /**
     * @brief Permet de construire un mur devant le personnage, ou bien de l'enlever s'il y a déjà un mur
    */
    void ConstruireDir(); //Construit ou détruit le mur devant le perso
};

#endif
