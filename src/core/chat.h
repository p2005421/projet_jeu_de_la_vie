#ifndef _CHAT
#define _CHAT

#include <string>
#include <vector>
#include <iostream>
#include "grille.h"
#include <cassert>

class Chat {
private:
    std::vector<int> pos;
    //std::vector<int> dir;
public: 

    Chat();
    ~Chat();

    void mvtChat(const Grille& g);
    //void afficherPos();
    //void setPos(std::vector<int> mvt);
    

    int getXChat() const;
    int getYChat() const;



    void mvtChatconst (Grille & g);
};

#endif