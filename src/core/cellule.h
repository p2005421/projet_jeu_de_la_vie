#ifndef _CELLULE
#define _CELLULE
#include <cassert>
class Cellule
{
private:
    char etat; //!< \brief type de la cellule
public:

    Cellule();
    Cellule(char e);
    ~Cellule();
    
    /**
     * @brief Modifie l'état de la cellule 
     * @param e le caractère resprésentant le nouvel état de la cellule
    */
    void setEtat(char e);

    /**
     * @brief Retourne l'état de la cellule
    */
    char getEtat() const;

    
    void incrementeTemps();
    void testRegressionCellule();
};
#endif
