#ifndef _GRILLE
#define _GRILLE

#include "cellule.h"

int const MAX = 500;

class Grille
{
private:
    /**
     * @brief Longueur de la grille
    */
    int dimX;
    
    /**
     * @brief Largeur de la grille
    */
    int dimY;//!< \brief hauteur
    
    /**
     * @brief Tableau de cellule composant la grille
    */
    Cellule tab[MAX][MAX]; //!< \brief tableau de cellules composant la grille
public:
    /**
     * @brief Constructeur de la classe Grille
    */
    Grille();
    
    /**
     * @brief Destructeur de la classe Grille
    */
    ~Grille();//!< \brief Destructeur
    
    /**
     * @brief Retourne la longeur de la grille
    */
    int getX() const;
    
    /**
     * @brief Retourne la largeur de la grille
    */
    int getY() const;
    
    /**
     * @brief Retourne une cellule de la grille selon ses coordonnées
     * @param x coordonnée de la cellule à modifier sur l'axe des abscisses
     * @param y coordonnée de la cellule à modifier ur l'axe des ordonées
    */
    Cellule getXY(const int x, const int y) const;
    
    /**
     * @brief Modifie l'état d'une cellule de la grille
     * @param x coordonnée de la cellule à modifier sur l'axe des abscisses
     * @param y coordonnée de la cellule à modifier ur l'axe des ordonées
     * @param e nouvelle état de la cellule de coordonnées (x,y)
    */
    void setGrille (int x, int y, char e);
    
    /**
     * @brief Retourne l'état d'une cellule de la grille
     * @param x coordonnée de la cellule à tester sur l'axe des abscisses
     * @param y coordonnée de la cellule à tester sur l'axe des ordonées
    */
    char getGrille(int x, int y) const;
    
    /**
     * @brief Retourne si oui ou non le personnage peut marcher sur une case de la grille
     * @param x coordonnée de la cellule à tester sur l'axe des abscisses
     * @param y coordonnée de la cellule à tester sur l'axe des ordonées
    */
    bool estValide(const int x, const int y) const;
    
    /**
     * @brief Retourne si oui ou non le personnage peut interagir avec une case de la grille
     * @param x coordonnée de la cellule à tester sur l'axe des abscisses
     * @param y coordonnée de la cellule à tester sur l'axe des ordonées
    */
    bool peutInteragir(const int x, const int y) const;
    
    void testRegressionGrille();
};
#endif
