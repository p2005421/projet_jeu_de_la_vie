#include <iostream>
#include "jeu.h"
#include <chrono>
#include <thread>
#include <unistd.h>

using namespace std;

Jeu::Jeu () : terrain(), perso(), chat() {
	perso.testRegressionPerso();
}

void Jeu::EtatSuivCtrl (const char touche) {
	switch(touche) {
		case 'q' :
				perso.gauche(terrain);
				break;
		case 'd' :
				perso.droite(terrain);
				break;
		case 'z' :
				perso.haut(terrain);
				break;
		case 's' :
				perso.bas(terrain);
				break;
		case 'e' :
				if (terrain.getGrille(perso.getX(),perso.getY())=='R')
				{
					Dormir();
				}

				if (terrain.getGrille(perso.getX(),perso.getY())=='C')
				{
					seDoucher();
				}

				if (terrain.getGrille(perso.getX(),perso.getY())=='T')
				{
					allerToilettes();
				}
				if (terrain.getGrille(perso.getDirX(),perso.getDirY())=='B')
				{
					Biblio();
				}
				if (terrain.getGrille(perso.getDirX(),perso.getDirY())=='L')
				{
					LaverMain();
				}
				if (terrain.getGrille(perso.getDirX(),perso.getDirY())=='N')
				{
					Mange();
				}
				if (terrain.getGrille(perso.getDirX(),perso.getDirY())=='D')
				{
					seDivertir();
				}
				if (perso.getDirX()==chat.getXChat() && perso.getDirY()==chat.getYChat())
				{
					CaresseChat();
				}
				if (terrain.getGrille(perso.getDirX(),perso.getDirY())=='S')
				{
					Boire();
				}
				if (terrain.getGrille(perso.getDirX(), perso.getDirY())=='E')
				{
					Travail();
				}
				if (terrain.getGrille(perso.getDirX(), perso.getDirY())=='M')
				{
					seSoigner();
				}
				break;
		case 'c' :
				ConstruireDir();
				break;
		case 'x' :
				Construire();
				break;
	}
}

void Jeu::EtatSuivAuto(const char touche) {
	switch(touche) {
		case 'k' :
			perso.mvtAuto(terrain);
			break;
	}
}

void Jeu::Dormir()
{
    if (terrain.getGrille(perso.getX(),perso.getY())=='R')
    {
        if (perso.getSommeil()<100)
		{
			perso.setSommeil(100);
			cout<<"Vous avez bien dormi !"<<endl
			<<"Votre sommeil passe à "<<perso.getSommeil()<<endl;
		}
		else cout<<"Vous n'êtes pas fatigué"<<endl;
    }
}

void Jeu::seDoucher()
{
    if (terrain.getGrille(perso.getX(),perso.getY())=='C')
    {
        if (perso.getHygiene()<100)
		{
			perso.setHygiene(100);
			cout<<"Vous êtes très propre !"<<endl
			<<"Votre hygiène passe à "<<perso.getHygiene()<<endl;
		}
		else cout<<"Vous êtes déjà propre"<<endl;
    }
}

void Jeu::allerToilettes()
{
    if (terrain.getGrille(perso.getX(),perso.getY())=='T')
    {
        if (perso.getHygiene()<100)
		{
			perso.setHygiene(perso.getHygiene()+20);
			if (perso.getHygiene()>100)
			{
				perso.setHygiene(100);
			}
			cout<<"Vous avez fait vos besoin !"<<endl
			<<"Votre hygiène passe à "<<perso.getHygiene()<<endl;
		}
		else cout<<"Vous êtes déjà propre"<<endl;
    }
}

void Jeu::Mange()
{
	if(terrain.getGrille(perso.getDirX(),perso.getDirY())=='N')
	{
		if (perso.getFaim()<99)
		{
			perso.setFaim(perso.getFaim()+20);
			if (perso.getFaim()>100)
			{
				perso.setFaim(100);
			}
			cout<<"Vous avez mangé !"<<endl
			<<"Votre faim passe à "<<perso.getFaim()<<endl;
			//NourritureApp();
		}
		else cout<<"Vous n'avez pas faim"<<endl;

	}
	else cout<<"Il n'y a rien à manger aux alentours..."<<endl;
}


void Jeu::Boire()
{
	if(terrain.getGrille(perso.getDirX(),perso.getDirY())=='S')
	{
		if (perso.getSoif()<99)
		{
			perso.setSoif(perso.getSoif()+20);
			if (perso.getSoif()>100)
			{
				perso.setSoif(100);
			}
			cout<<"Vous avez bu de l'eau !"<<endl
			<<"Votre soif passe à "<<perso.getFaim()<<endl;
		}
		else cout<<"Vous n'avez pas soif"<<endl;

	}
	else cout<<"Il n'y a rien à boire aux alentours..."<<endl;
}

void Jeu::seDivertir()
{
	if(terrain.getGrille(perso.getDirX(),perso.getDirY())=='D')
	{
		if (perso.getMental()<99)
		{
			perso.setMental(perso.getMental()+20);
			if (perso.getMental()>100)
			{
				perso.setMental(100);
			}
			cout<<"Vous vous amusez comme un petit fou!"<<endl
			<<"Votre état mental passe à "<<perso.getMental()<<endl;
		}
		else cout<<"Vous n'avez pas besoin de divertissement"<<endl;

	}
	else cout<<"Il n'y a pas de source divertissement aux alentours..."<<endl;
}

void Jeu::Biblio()
{
	if(terrain.getGrille(perso.getDirX(),perso.getDirY())=='B')
	{
		if (perso.getMental()<99)
		{
			perso.setMental(perso.getMental()+20);
			if (perso.getMental()>100)
			{
				perso.setMental(100);
			}
			cout<<"Vous lisez un bon livre"<<endl
			<<"Votre état mental passe à "<<perso.getMental()<<endl;
		}
		else cout<<"Vous n'avez pas besoin de divertissement"<<endl;

	}
	else cout<<"Il n'y a pas de source divertissement aux alentours..."<<endl;
}


void Jeu::LaverMain()
{
    if (terrain.getGrille(perso.getDirX(),perso.getDirY())=='L')
    {
        if (perso.getHygiene()<100)
		{
			perso.setHygiene(perso.getHygiene()+20);
			if (perso.getHygiene()>100)
			{
				perso.setHygiene(100);
			}
			cout<<"Vous avez fait vos besoin !"<<endl
			<<"Votre hygiène passe à "<<perso.getHygiene()<<endl;
		}
		else cout<<"Vous êtes déjà propre"<<endl;
    }
}

void Jeu::Travail()
{
	if((terrain.getGrille(perso.getDirX(), perso.getDirY()) == 'E')) 
	{
		if (perso.getMental()>0)
		{
			perso.setMental(perso.getMental()-1);
		}
		else if (perso.getMental()==0)
		{
			perso.setMental(0);
		}
	}


}

void Jeu::Demence(int temps, int vitesse) 
{
	if(temps%vitesse*3 == 0 && perso.getMental() == 0)
	{
		perso.mvtAuto(getGrille());
		std::cout << "Votre perso. est atteint de démence, il est incontrolable !" << std::endl;
	}
}

void Jeu::CaresseChat()
{

		if (perso.getMental()<99)
		{
			perso.setMental(perso.getMental()+20);
			if (perso.getMental()>100)
			{
				perso.setMental(100);
			}
			cout<<"Vous caressez votre chat cela vous réconforte"<<endl
			<<"Votre état mental passe à "<<perso.getMental()<<endl;
		}
		else cout<<"Vous n'avez pas besoin de divertissement"<<endl;


}


const Grille& Jeu::getGrille() const {
	return terrain;
}

const Personnage& Jeu::getPerso() const {
	return perso;
}

void Jeu::Construire()
{
	int x;
	int y;
	char c;
	do
	{
		cout<<"Coordonnée X : \n";
		cin>>x;
	}
	while (x>terrain.getX() || x<0);

	do
	{
		cout<<"Coordonnée Y : \n";
		cin>>y;
	}
	while (y>terrain.getY() || y<0);

	cout<<"Type de cellule \n";
	cin>>c;
	if (x==perso.getX() && y==perso.getY())
	{
		cout<<"Tu es déjà sur cette case ! Pousses toi\n";
	}
	else if (terrain.getGrille(x,y)=='_')
	{
		terrain.setGrille(x,y,c);
	}
	else cout<<"Construction impossible \n";
} 

void Jeu::ConstruireDim(int x,int x1,int y,int y1,char c)
{
	cout<<"Coordonnée X : \n";
	cin>>x;
	cout<<"Coordonnée Y : \n";
	cin>>y;
	cout<<"Type de cellule \n";
	cin>>c;
}

void Jeu::EcouleSommeil(int temps, int vitesse)
{
	if (temps%vitesse==0)
		{
			if (perso.getSommeil()>0) 
			{
				perso.setSommeil(perso.getSommeil()-1); 
			}
		}
}

void Jeu::EcouleSoif(int temps, int vitesse)
{
	if (temps%vitesse==0)
		{
			if (perso.getSoif()>0) 
			{
				perso.setSoif(perso.getSoif()-1); 
			}
		}
}

void Jeu::EcouleFaim(int temps, int vitesse)
{
	if (temps%vitesse==0)
		{
			if (perso.getFaim()>0) 
			{
				perso.setFaim(perso.getFaim()-1); 
			}
		}
}

void Jeu::EcouleMental(int temps, int vitesse)
{
	if (temps%vitesse==0)
		{
			if (perso.getMental()>0) 
			{
				perso.setMental(perso.getMental()-1); 
			}
		}
	if (perso.getMental()==0)
	{
		Demence(temps, vitesse);
	}
}

void Jeu::EcouleHygiene(int temps, int vitesse)
{
	if (temps%vitesse==0)
		{
			if (perso.getHygiene()>0) 
			{
				perso.setHygiene(perso.getHygiene()-1); 
			}
		}
}

void Jeu::EcouleSante(int temps, int vitesse)
{
	if (perso.getSommeil()<25 || perso.getSoif()<25 || perso.getMental()<25
	 || perso.getHygiene()<25 || perso.getFaim()<25)
		{
		if (temps%vitesse==0)
			{
				if (perso.getSante()>0) 
				{
					perso.setSante(perso.getSante()-1); 
				}
				if (perso.getSante()<0) {
					perso.setSante(0);
				}
			}
		}
}

const Grille& Jeu::getConstTerrain () const
{
    return terrain;
}

const Personnage& Jeu::getConstPerso () const
{
    return perso;
}

void Jeu::DeplacerChat(int temps, int vitesse) 
{
	if (temps%vitesse==0)
	{
		chat.mvtChat(terrain);
	}
}

const Chat& Jeu::getChat() const {
	return chat;
}

void Jeu::seBlesse()
{
	//std::cout<<perso.getSante()<<std::endl;
	if (perso.getBlesse()%4==0)
	{
		if (perso.getSante()>9)
		{
			perso.setSante(perso.getSante()-9);

		}
		else
			{
			perso.setSante(0);
			//std::cout<<"vie inferieur a 0 \n";
			}
		std::cout<<"Vous vous etes blessé en vous cognant trop de fois"<<std::endl;
		perso.setBlesse(1);
	}
	
}

bool Jeu::Mort() {
    if (perso.getSante()<=0) return true;
    else return false;
}

void Jeu::FinJeu() {
	std::cout << "Fin du jeu, votre personnage est mort !" << std::endl;
	//sleep(1);
	system("clear");
}

void Jeu::Maladie(int temps, int maladietps, int vitesse) {
	int chance_maladie;
	if (temps%maladietps==0) {
		chance_maladie = rand()%100+1;
		//cout<<chance_maladie<<endl;
	}
	if (chance_maladie==5) {
		perso.setMalade(true);
		std::cout << "Votre perso est malade" << std::endl;
	}
	if (perso.getMalade()) {		
		if (temps%vitesse==0)
			{
				if (perso.getSante()>0) 
				{
					perso.setSante(perso.getSante()-1);
					cout<<"vie perdue car malade"<<endl;
				}
				if (perso.getSante()<=0) {
					perso.setSante(0);
				}
			}
	
	}
}

void Jeu::seSoigner() {

if(terrain.getGrille(perso.getDirX(),perso.getDirY())=='M')
	{
		if (perso.getMalade() && perso.getSante()<100) {

			perso.setMalade(false);
			perso.setSante(perso.getSante()+1);
			SoinApp();
			std::cout << "Vous vous êtes soigné et guéri de votre rhume" << std::endl;

		} else if (perso.getSante()<100) {

			perso.setSante(perso.getSante()+5);
			SoinApp();

		} else if (perso.getSante()>=100) {

			perso.setSante(100);
			std::cout << "Vous êtes déjà au top !" << std::endl;
		}


	}
}


void Jeu::SoinApp()
{

	int randx,randy;
	terrain.setGrille(perso.getDirX(),perso.getDirY(),'_');
	do
	{
		randx=rand()%terrain.getX()+1;
		randy=rand()%terrain.getY()+1;
	} while (terrain.getGrille(randx,randy)!='_');

	terrain.setGrille(randx, randy,'M');
	//cout<<"la case a apparu en"<<randx<<' '<<randy<<endl;

	
}

void Jeu::ConstruireDir()
{
	if (terrain.getGrille(perso.getDirX(),perso.getDirY())=='_')
	{
		terrain.setGrille(perso.getDirX(), perso.getDirY(), '#');
		//cout<<"bloc \n";
	}

	else if (terrain.getGrille(perso.getDirX(),perso.getDirY())=='#')
	{
		terrain.setGrille(perso.getDirX(), perso.getDirY(), '_');
		//cout<<"terre \n";
	}
}