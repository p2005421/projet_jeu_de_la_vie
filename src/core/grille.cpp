#include <iostream>
#include <cassert>
#include "grille.h"
//#include "cellule.h"

Grille::Grille()
{
    dimX = 30;
    dimY = 30;
 
    unsigned i, j;
    
    for(i=0;i<dimX;i++)
        {
            for(j=0;j<dimY;j++)
            {
                tab[i][j].setEtat('_');
                
                if((j == 4 && i>3 && i<25) || (j == 24 && i<25 && i>3) || (i==4 && j>3 && j<25) || (i==24 && j<25 && j>3) // murs extérieurs
                   || (j==10 && i<13 && i>3 ) || (j<11 && i==12 && j>4)    // chambre 1
                   || (j==10 && i>15 && i<25) || (j<17 && i==16 && j>3) || (j==16 && i>15 && i<25) //chambre 2 & 3
                   || (j==14 && i<11 && i>3) || (j==18 && i<11 && i>3) || (j>9 && j<19 && i==10) //SDB & biblio
                   || (j==16 && i==10) || (j==18 && i<13 && i>9) || (j<21 && j>17 && i==12) // cuisine & WC
                   || (i==20 && j<13 && j>9) || (j==12 && i>19 && i<25 ) // sdb2
                   
                   )
                {
                    tab[i][j].setEtat('#'); //murs
                }
                
                if((j== 6 && i== 12) || (j== 5 && i== 16) //portes chambres 1 et 2
                   || (j== 15 && i== 16) || (j== 12 & i== 10) // portes chambre 3 & biblio
                   || (j== 12 && i== 22) || (j==4 && i== 14)
                   || (j== 15 & i== 10) || (j== 17 & i== 12) // portes sdb et wc
                   || (j== 24 && i>12 && i<17))
                {
                    tab[i][j].setEtat('_'); //portes
                }
                
                if((i<8 && i>4 && j<8 && j>4) || (i<21 && i>16 && j<10 && j>6) // chambres 1 et 2
                   || (i<20 && i>16 && j<14 && j>10) || (i<10 && i>7 && j==21) ||
                   (i==7 && j==23) || (i==23 && j==23) || (i==11 && j==8) || (i==23 && (j==6 || j==22)) || (i<24 && i>20 && j==17) || (i==23 && j<20 && j>17))
                {
                    tab[i][j].setEtat('R'); // lits, canapés,
                }
                
                if ((i==23 && j==11) || (i<10 && i>7 && j==17))
                {
                    tab[i][j].setEtat('C'); // cabines de douche
                }
                                
                if(i==5 && j==15)
                {
                    tab[i][j].setEtat('T'); // toilettes
                }

                if((i>4 && i<10 && j==11) || (i>4 && i<10 && j==13) || (i==5 && j==12)) 
                {
                    tab[i][j].setEtat('B'); // bibliothèques
                }

                if((i<12 && i>9 && j==9) || (i>21 && i<24 && j==5) || (i==19 && j==17) ||
                   (i==20 && j<21 && j>17) || (i<24 && i>21 && j==23) || (i==17 && j==23) || (i==11 && j==17))
                {
                    tab[i][j].setEtat('D'); // divertissement
                }

                if ((((i==6)||(i==11)||(i==9)) && j==19) || ((i<10 && i>7) && j==22) || (i==8 && j==23))
                {
                    tab[i][j].setEtat('N'); // nourriture
                }
                if ((i<9 && i>6 && j<20 && j>18) || (i==5 && j==17) || (i==21 && j==11))
                {
                    tab[i][j].setEtat('S'); // eau pour boire
                }
                if (i==6 && j==17) {
                    tab[i][j].setEtat('M');
                }
            }
        }
}


Grille::~Grille()
{
    for (unsigned i = 0; i < dimX; i++)
    {
        for (unsigned j = 0; j < dimY; j++)
        {
            tab[i][j].setEtat('_');
        }
    }
}


int Grille::getX() const
{
    return dimX;
}

int Grille::getY() const
{
    return dimY;
}

Cellule Grille::getXY(const int x, const int y) const {
    assert(x>=0);
    assert(y>=0);
    assert(x<dimX);
    assert(y<dimY);
    return getGrille(x,y);
}

void Grille::setGrille(int x, int y, char e)
{
    tab[x][y].setEtat(e);
}

char Grille::getGrille(int x, int y) const
{
    return tab[x][y].getEtat();
}

bool Grille::estValide(int const x, int const y) const 
{
    return ((tab[x][y].getEtat() == '_') || (tab[x][y].getEtat() == 'R') ||
            (tab[x][y].getEtat() == 'C') || (tab[x][y].getEtat() == 'T')
            );
}

bool Grille::peutInteragir(int const x, int const y)const
{
    return ((tab[x][y].getEtat() == '#') || (tab[x][y].getEtat() == 'N') ||
            (tab[x][y].getEtat() == 'E') || (tab[x][y].getEtat() == 'D')
            || (tab[x][y].getEtat() == 'L') || (tab[x][y].getEtat() == 'B')
            || (tab[x][y].getEtat() == 'E'));
}
    
void Grille::testRegressionGrille()
{
    Grille g;
    
    assert(g.dimX == 30 && g.dimY == 30);
    assert(g.getX() == 30);
    assert(g.getY() == 30);
    
    assert(g.getGrille(4,12) == '#');
    assert(g.getGrille(7,12) == '_');
    assert(g.getGrille(23,11) == 'C');
    assert(g.getGrille(5,17) == 'T');
    assert(g.getGrille(5,12) == 'B');
    
    g.setGrille(4,12,'O');
    assert(g.getGrille(4,12) == 'O');
    
    assert(g.estValide(7,12) == true);
    assert(g.estValide(4,12) == false);
    
}
