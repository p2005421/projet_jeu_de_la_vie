#include <iostream>
#include "cellule.h"
#include <cassert>

Cellule::Cellule()
{
    etat = 'S';
}

Cellule::Cellule(char e)
{
    etat = e;
}

Cellule::~Cellule()
{
    etat = 'S';
}

void Cellule::setEtat(char e)
{
    etat = e;
}

char Cellule::getEtat() const
{
    return etat;
}


void Cellule::testRegressionCellule()
{
    Cellule c1;
    Cellule c2('R');
    
    assert(c1.etat == 'S');
    
    assert(c2.etat == 'R');
    
    c1.setEtat('O');
    assert(c1.etat == 'O');
    
    assert(c1.getEtat() == 'O');
    
    
}
