/**
@brief Module permettant de générer un personnage qui sera jouable

@file /core/perso.h
*/

#ifndef _PERSO
#define _PERSO

#include <string>
#include <vector>
#include <iostream>
#include "grille.h"
#include <cassert>

/**
 * @brief La classe "Personnage" contenant ses besoins (faim, soif...) avec une valeur maximum de 100,
 * sa position et direction (vecteurs 2D) ainsi que les variables de compteur de blessure et maladie (booléen)
*/
class Personnage {
private:

    unsigned int faim, soif, sommeil, etatMental, hygiene, sante;
    std::vector<int> pos,dir;
    unsigned int countblesse;
    bool malade;

public: 
    std::string direction;

    /**
     * @brief Construit un objet "Personnage" en initialisant tous les besoins à 50,
     * sa position et direction en [0,0].
    */
    Personnage();

    /**
     * @brief Construit un personnage avec les valeurs en besoins prédéfinis en paramètres
     * et une position / direction en [0,0].
    */
    Personnage(int faim_v, int soif_v, int sommeil_v, int etatMental, int hygiene_v, int sante_v);

    /**
     * @brief Détruit un personnage, réinitialisant tous ses besoins à 0.
    */
    ~Personnage();

    /**
     * @brief Permet de connaître l'état de sommeil du personnage
    */
    unsigned int getSommeil() const;

    /**
     * @brief Permet de connaître l'état de faim du personnage
    */
    unsigned int getFaim() const;

    /**
     * @brief Permet de connaître l'état de soif du personnage
    */
    unsigned int getSoif() const;

    /**
     * @brief Permet de connaître l'état mental du personnage
    */
    unsigned int getMental() const;

    /**
     * @brief Permet de connaître l'hygiène du personnage
    */
    unsigned int getHygiene() const;

    /**
     * @brief Permet de connaître la santé du personnage
    */
    unsigned int getSante() const;

    /**
     * @brief Permet de connaître le compteur de blessure du personnage
    */
    unsigned int getBlesse() const;

    /**
     * @brief Permet de savoir si le personnage est malade ou non
    */
    bool getMalade() const;




    /**
     * @brief Modifie la valeur de sommeil du personnage
     * @param valSommeil la valeur de sommeil à appliquer
    */
    void setSommeil(int valSommeil);

    /**
     * @brief Modifie la valeur de faim du personnage
     * @param valFaim la valeur de faim à appliquer
    */
    void setFaim(int valFaim);

    /**
     * @brief Modifie la valeur de soif du personnage
     * @param valSoif la valeur de soif à appliquer
    */
    void setSoif(int valSoif);

    /**
     * @brief Modifie l'état mental (valeur) du personnage
     * @param valMental la valeur de l'état mental à appliquer
    */
    void setMental(int valMental);

    /**
     * @brief Modifie l'hygiène (valeur) du personnage
     * @param valHyg la valeur d'hygiène à appliquer
    */
    void setHygiene(int valHyg);

    /**
     * @brief Modifie la santé (valeur) du personnage
     * @param valSante la valeur de santé à appliquer
    */
    void setSante(int valSante);

    /**
     * @brief Modifie le compteur de blessure du personnage
     * @param countblesse la valeur du compteur à appliquer
    */
    void setBlesse(int countblesse);

    /**
     * @brief Modifie l'état de maladie du personnage
     * @param test Etat du test à appliquer pour la maladie
    */
    void setMalade(bool test);




    /**
     * @brief Fonction qui permet au personnage de se déplacer vers la droite
     * @param g La grille de terrain sur laquelle se trouve le personnage
    */
    void droite(const Grille & g);

    /**
     * @brief Fonction qui permet au personnage de se déplacer vers la gauche
     * @param g La grille de terrain sur laquelle se trouve le personnage
    */
    void gauche(const Grille & g);

    /**
     * @brief Fonction qui permet au personnage de se déplacer vers le haut
     * @param g La grille de terrain sur laquelle se trouve le personnage
    */
    void haut(const Grille & g);

    /**
     * @brief Fonction qui permet au personnage de se déplacer vers le bas
     * @param g La grille de terrain sur laquelle se trouve le personnage
    */   
    void bas(const Grille & g);

    /**
     * @brief Fonction qui permet un mouvement aléatoire du personnage
     * @param g La grille de terrain sur laquelle se trouve le personnage
    */
    void mvtAuto(const Grille & g);

    /**
     * @brief Affiche les coordonnées X,Y du personnage
    */
    void afficherPos();

    /**
     * @brief Récupère la coordonnée horizontale du personnage
    */
    int getX() const;

    /**
     * @brief Récupère la coordonnée verticale du personnage
    */
    int getY() const;

    /**
     * @brief Récupère la dernière direction horizontale du personnage
    */
    int getDirX() const;

    /**
     * @brief Récupère la dernière direction verticale du personnage
    */
    int getDirY() const;

    /**
     * @brief Tests de regression sur la classe "Personnage"
    */
    void testRegressionPerso();

};

#endif