/** \mainpage Projet Jeu de la vie - Sims"


\section Introduction

Documentation du projet Jeu de la Vie - Sims. <br>
Groupe composé de BOUDEFAR Jounaid, MARTINEZ Zoé et LE Alexandre.

<br>

\section Compilation

$ make => pour générer les deux éxécutables pour l'affichage txt et sous sdl2 <br>
$ make clean => pour effacer tous les fichiers générés

Dépendance : SDL2 : http://www.libsdl.org
<br> <br>

\section Execution
$ ./bin/sd => Pour l'éxécutable sdl
<br>
$ ./bin/m => Pour l'éxécutable txt 
<br>

en fonction des exécutables générés
<br> <br>

\section Génération de la documentation

 $ make doc ou alors $ cd doc puis "doxygen sims.doxy" <br>
 Puis ouvrir doc/html/index.html avec le navigateur de votre choix

Dans cette documentation, sont documentés les modules "Grille", "Cellule", "Personnage" et "Jeu"

Dépendance : 
<br>
doxygen : http://www.stack.nl/~dimitri/doxygen/ <br>

*/
