#include <iostream>
#include "chat.h"
//#include "inventaire.h"

Chat::Chat() {
    pos.assign(2,15);
}

Chat::~Chat() {

}

int Chat::getXChat() const {
    return pos[0];
}

int Chat::getYChat() const {
    return pos[1];
}


void Chat::mvtChat(const Grille& g) {
    int rpx, rpy;
    rpy = rand()%3-1;
    rpx = rand()%3-1;
    do {
        rpy = rand()%3-1;
        rpx = rand()%3-1;
    } while (!g.estValide(pos[0]+rpx,pos[1]+rpy));
        pos[0] = pos[0] + rpx;
        pos[1] = pos[1] + rpy;

}