#ifndef WINTXT_H
#define WINTXT_H
#include "cellule.h"
#include "grille.h"

class WinTXT
{
private:

    int dimx;       
    int dimy;       
    Grille* win;      

public:

    WinTXT (int dx, int dy);
    void clear (char c=' ');
    void print (int x, int y, Cellule c);
    void print (int x, int y, Cellule* c);
    void draw (int x=0, int y=0);
    void pause();
    char getCh();

};

void termClear ();

#endif