#include <iostream>
#ifdef _WIN32
#include <windows.h>
#else
#include <unistd.h>
#endif // WIN32
#include "winTxt.h"

#include "jeu.h"
#include <time.h>
using namespace std;

void txtAff(WinTXT & win, const Jeu & jeu) {
	const Grille& terrain = jeu.getGrille();
	const Personnage& perso = jeu.getPerso();
	const Chat& chat = jeu.getChat();

	win.clear();

	for(int x=0;x<terrain.getX();++x)
		for(int y=0;y<terrain.getY();++y)
			win.print(x,y,terrain.getXY(x,y));


	win.print(perso.getX(),perso.getY(),'P');

	win.print(chat.getXChat(),chat.getYChat(),'c');

	win.draw();
}

void txtBoucle (Jeu & jeu) {
	
    WinTXT win (jeu.getGrille().getX(),jeu.getGrille().getY());
	bool ctrl = false;
	bool ok = true;
	int c;
	int temps = clock();

	do {
	    txtAff(win,jeu);
		jeu.perso.testRegressionPerso();
		temps = ((int)clock()/1000) /*- temps ecoulé*/;
		//cout<<temps<<endl<<jeu.chat.getXChat()<<endl;

		int vitesse=10; /* vitesse d ecoulement*/
		jeu.EcouleSommeil(temps, vitesse);
		jeu.EcouleSoif(temps, vitesse);
    	jeu.EcouleFaim(temps, vitesse);
    	jeu.EcouleHygiene(temps, vitesse);
    	jeu.EcouleSante(temps, vitesse);
		jeu.EcouleMental(temps, vitesse);
		jeu.DeplacerChat(temps,vitesse);
		jeu.Maladie(temps, 100, vitesse);
        jeu.seBlesse();


        jeu.perso.afficherPos();
		
        #ifdef _WIN32
        Sleep(100);
		#else
		usleep(100000);
        #endif // WIN32
		c = win.getCh();

		if (ctrl) {
			if (temps%50==0)
			{
			jeu.EtatSuivAuto('k');
			}
			//jeu.EtatSuivCtrl('l');
			switch (c) {

				case 'l' :
					ctrl = false;
					break;
				}
		} else {
			switch (c) {
			case 'q':
				jeu.EtatSuivCtrl('q');
				break;
			case 'd':
				jeu.EtatSuivCtrl('d');
				break;
			case 's':
				jeu.EtatSuivCtrl('z');
				break;
			case 'z':
				jeu.EtatSuivCtrl('s');
				break;
			case 'e':
				jeu.EtatSuivCtrl('e');
				break;
			case 'c':
				jeu.EtatSuivCtrl('c');
				break;
			case 'x':
				jeu.EtatSuivCtrl('x');
				break;
			case 'k':
				ctrl = true;
				break;
			case 'o':
				ok = false;
				break;
			}
		}

		if (jeu.Mort())
		{
			ok=false;
		}

		

	} while (ok);

}
