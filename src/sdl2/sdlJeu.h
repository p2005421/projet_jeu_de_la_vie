#ifndef _SDLJEU_H
#define _SDLJEU_H

#include <SDL.h>
#include <SDL_ttf.h>
#include <SDL_image.h>
#include <SDL_mixer.h>


#include "jeu.h"

class Image
{
private:
    SDL_Surface * surface;
    SDL_Texture * texture;
    bool has_changed;
    
public:
    Image();
    void loadFromFile (const char* filename, SDL_Renderer * renderer);
    void loadFromCurrentSurface (SDL_Renderer * renderer);
    void draw (SDL_Renderer * renderer, int x, int y, int w=-1, int h=-1);
    SDL_Texture * getTexture() const;
    void setSurface(SDL_Surface * surf);
};


class sdlJeu
{
private:
    SDL_Window * window;
    SDL_Renderer * renderer;

    /**
     * @brief "Faim :"
    */
    Image font_txtFaim;
    
    /**
     * @brief Faim du personnage
    */
    Image font_faim;
    Image font_txtSoif;
    Image font_soif;
    Image font_txtSommeil;
    Image font_sommeil;
    Image font_txtMental;
    Image font_mental;
    Image font_txtHygiene;
    Image font_hygiene;
    Image font_txtSante;
    Image font_sante;
    Image font_sommeilMess;
    Image font_hygieneMess;
    Image font_bibliMess;
    Image font_chatMess;
    Image font_vide;
    SDL_Color font_color;
    
    /**
     * @brief Personnage qui regarde vers la droite
    */
    Image im_persoD;
    
    /**
     * @brief Personnage qui regarde vers le bas
    */
    Image im_persoB;
    
    /**
     * @brief Personnage qui regarde vers la droite
    */
    Image im_persoG;
    
    /**
     * @brief Personnage qui regarde vers le haut
    */
    Image im_persoH;
    
    /**
     * @brief Mur
    */
    Image im_mur;
    
    /**
     * @brief Parquet
    */
    Image im_sol;
    
    /**
     * @brief Sol de la cuisine
    */
    Image im_cuisine;
    
    /**
     * @brief Sol de la salle de bain
    */
    Image im_sdb;
    
    /**
     * @brief Lit 1
    */
    Image im_litCH1;
    
    /**
     * @brief Lit 2
    */
    Image im_litCH2;
    
    /**
     * @brief Lit 3
    */
    Image im_litCH3;
    
    /**
     * @brief Bac de douche
    */
    Image im_douche;
    
    /**
     * @brief Chat
    */
    Image im_chat;
    
    /**
     * @brief Canapé
    */
    Image im_canape;
    
    /**
     * @brief Chaise de bureau 1
    */
    Image im_chaise;
    
    /**
     * @brief Frigo
    */
    Image im_frigo;
    
    /**
     * @brief Gazinière
    */
    Image im_gaziniere;
    
    /**
     * @brief Lavabo 1
    */
    Image im_lavabo1;
    
    /**
     * @brief Lavabo 2
    */
    Image im_lavabo2;
    
    /**
     * @brief Evier
    */
    Image im_evier;
    
    /**
     * @brief Ordinateur bureau
    */
    Image im_ordi;
    
    /**
     * @brief Plante
    */
    Image im_plante;
    
    /**
     * @brief Table à manger, meubles de cuisine, bureaux, bibliothèques
    */
    Image im_table;
    
    /**
     * @brief Télévision
    */
    Image im_tv;
    
    /**
     * @brief Tabouret de cuisine
    */
    Image im_tabouret;
    
    /**
     * @brief Baignoire
    */
    Image im_baignoire;
    
    /**
     * @brief Sol jardin
    */
    Image im_herbe;
    
    /**
     * @brief Boîte à pharmacie
    */
    Image im_soin;
    
    /**
     * @brief Assiette repas
    */
    Image im_repas;
    
    
    /**
     * @brief Livre
    */
    Image im_livre1;
    
    /**
     * @brief Toilette
    */
    Image im_toilette;
    
    /**
     * @brief Tapis bibliothèque
    */
    Image im_tapis;
    
    bool souris;
    bool touche;

public:

    /**
     * @brief Jeu qui sera affcihé
    */
    Jeu jeu;
    
    /**
     * @brief Police
    */
    TTF_Font * font;
    
    /**
     * @brief Constructeur de la classe sdlJeu
    */
    sdlJeu();
    
    /**
     * @brief Destructeur de la classe sdlJeu
    */
    ~sdlJeu ();
    
    /**
     * @brief Boucle d'événement du jeu'
    */
    void sdlBoucle();
    
    /**
     * @brief Affichage du jeu
    */
    void sdlAffichage();

};

#endif
