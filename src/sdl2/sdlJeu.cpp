#include <cassert>
#include <functional>
#include <time.h>
#include "sdlJeu.h"
#include "jeu.h"
#include <stdlib.h>
#include <iostream>

const int zoom = 32;


Image::Image()
{
    surface = nullptr;
    texture = nullptr;
    has_changed = false;
}

void Image::loadFromFile (const char* filename, SDL_Renderer * renderer) {
    surface = IMG_Load(filename);
    if (surface == NULL) {
        std::string nfn = std::string("../") + filename;
        std::cout << "Error: cannot load "<< filename <<". Trying "<<nfn<<std::endl;
        surface = IMG_Load(nfn.c_str());
        if (surface == NULL) {
            nfn = std::string("../") + nfn;
            surface = IMG_Load(nfn.c_str());
        }
    }
    if (surface == NULL) {
        std::cout<<"Error: cannot load "<< filename <<std::endl;
        SDL_Quit();
        exit(1);
    }

    SDL_Surface * surfaceCorrectPixelFormat = SDL_ConvertSurfaceFormat(surface,SDL_PIXELFORMAT_ARGB8888,0);
    SDL_FreeSurface(surface);
    surface = surfaceCorrectPixelFormat;

    texture = SDL_CreateTextureFromSurface(renderer,surfaceCorrectPixelFormat);
    if (texture == NULL) {
        std::cout << "Error: problem to create the texture of "<< filename << std::endl;
        SDL_Quit();
        exit(1);
    }
}


void Image::loadFromCurrentSurface (SDL_Renderer * renderer) {
    texture = SDL_CreateTextureFromSurface(renderer,surface);
    if (texture == NULL) {
        std::cout << "Error: problem to create the texture from surface " << std::endl;
        SDL_Quit();
        exit(1);
    }
}


void Image::draw (SDL_Renderer * renderer, int x, int y, int w, int h) {
    int ok;
    SDL_Rect r;
    r.x = x;
    r.y = y;
    r.w = (w<0)?surface->w:w;
    r.h = (h<0)?surface->h:h;

    if (has_changed) {
        ok = SDL_UpdateTexture(texture,NULL,surface->pixels,surface->pitch);
        assert(ok == 0);
        has_changed = false;
    }

    ok = SDL_RenderCopy(renderer,texture,NULL,&r);
    assert(ok == 0);
}


SDL_Texture * Image::getTexture() const
{
    return texture;
}

void Image::setSurface(SDL_Surface * surf)
{
    surface = surf;
}

// ------------------------------------------------------ //

sdlJeu::sdlJeu (){
    // Initialisation de la SDL
    if (SDL_Init(SDL_INIT_VIDEO) < 0) {
        std::cout << "Erreur lors de l'initialisation de la SDL : " << SDL_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    } else {
    std::cout << "initialisation de la sdl OK" << std::endl;
    }

    if (TTF_Init() != 0) {
        std::cout << "Erreur lors de l'initialisation de la SDL_ttf : " << TTF_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    } else {
    std::cout << "initialisation sdl ttf OK" << std::endl;
    }

    int dimx, dimy;
	dimx = jeu.getConstTerrain().getX();
	dimy = jeu.getConstTerrain().getY();
	dimx = dimx * zoom;
	dimy = dimy * zoom;


    int imgFlags = IMG_INIT_PNG | IMG_INIT_JPG;
    if( !(IMG_Init(imgFlags) & imgFlags)) {
        std::cout << "Erreur lors de l'initilisation de la SDL_img " << IMG_GetError() << std::endl;
        SDL_Quit();
        exit(1);
    }
    else {
    std::cout << "initialisation sdl image OK" << std::endl;
    }

    window = SDL_CreateWindow("Sims", SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED, dimx, dimy, SDL_WINDOW_SHOWN | SDL_WINDOW_RESIZABLE);
    if (window == NULL) {
        std::cout << "Erreur lors de la creation de la fenetre : " << SDL_GetError() << std::endl; 
        SDL_Quit(); 
        exit(1);
    }


    renderer = SDL_CreateRenderer(window,-1,SDL_RENDERER_ACCELERATED);

     im_mur.loadFromFile("data/mur.png",renderer);
     im_sol.loadFromFile("data/sol.png",renderer);
     im_cuisine.loadFromFile("data/cuisine.jpg", renderer);
     im_sdb.loadFromFile("data/sdb.jpg",renderer);
     im_litCH1.loadFromFile("data/litCH1.png",renderer);
     im_litCH2.loadFromFile("data/litCH2.png",renderer);
     im_litCH3.loadFromFile("data/litCH3.png",renderer);
     im_douche.loadFromFile("data/douche.png",renderer);
     im_chat.loadFromFile("data/chat.png",renderer);
     im_persoB.loadFromFile("data/persoH.png",renderer);
     im_persoG.loadFromFile("data/persoG.png",renderer);
     im_persoH.loadFromFile("data/persoB.png",renderer);
     im_persoD.loadFromFile("data/persoD.png",renderer);
     im_gaziniere.loadFromFile("data/gaziniere.png", renderer);
     im_canape.loadFromFile("data/canape.png", renderer);
     im_chaise.loadFromFile("data/chaise.png", renderer);
     im_frigo.loadFromFile("data/frigo.png", renderer);
     im_lavabo1.loadFromFile("data/lavabo1.png", renderer);
     im_lavabo2.loadFromFile("data/lavabo2.png", renderer);
     im_evier.loadFromFile("data/evier.png",renderer);
     im_ordi.loadFromFile("data/ordi.png", renderer);
     im_plante.loadFromFile("data/plante.png", renderer);
     im_table.loadFromFile("data/table.png", renderer);
     im_tv.loadFromFile("data/tv.png", renderer);
     im_tabouret.loadFromFile("data/tabouret.png",renderer);
     im_baignoire.loadFromFile("data/baignoire.png",renderer);
     im_herbe.loadFromFile("data/herbe.jpg",renderer);
     im_soin.loadFromFile("data/soins.png", renderer);
     im_repas.loadFromFile("data/repas.png",renderer);
     im_livre1.loadFromFile("data/livres1.png",renderer);
     
     im_toilette.loadFromFile("data/toilette.png",renderer);
     im_tapis.loadFromFile("data/tapis.png",renderer);
     

    // FONTS
    font = TTF_OpenFont("data/DejaVuSansCondensed.ttf",50);
    if (font == NULL)
        font = TTF_OpenFont("../data/DejaVuSansCondensed.ttf",50);
    if (font == NULL) {
            std::cout << "Failed to load DejaVuSansCondensed.ttf! SDL_TTF Error: " << TTF_GetError() << std::endl; 
            SDL_Quit(); 
            exit(1);
	}
	font_color.r = 255;font_color.g = 255;font_color.b = 255;
	font_txtFaim.setSurface(TTF_RenderText_Solid(font,"Faim : ",font_color));
	font_txtFaim.loadFromCurrentSurface(renderer);
    font_txtSoif.setSurface(TTF_RenderText_Solid(font,"Soif : ",font_color));
	font_txtSoif.loadFromCurrentSurface(renderer);
    font_txtSommeil.setSurface(TTF_RenderText_Solid(font,"Sommeil : ",font_color));
	font_txtSommeil.loadFromCurrentSurface(renderer);
    font_txtMental.setSurface(TTF_RenderText_Solid(font,"Mental : ",font_color));
	font_txtMental.loadFromCurrentSurface(renderer);
    font_txtHygiene.setSurface(TTF_RenderText_Solid(font,"Hygiene : ",font_color));
	font_txtHygiene.loadFromCurrentSurface(renderer);
    font_txtSante.setSurface(TTF_RenderText_Solid(font,"Sante : ",font_color));
	font_txtSante.loadFromCurrentSurface(renderer);

    font_sommeilMess.setSurface(TTF_RenderText_Solid(font,"Vous avez bien dormi",font_color));
	font_sommeilMess.loadFromCurrentSurface(renderer);

    font_hygieneMess.setSurface(TTF_RenderText_Solid(font,"Vous etes tres propre",font_color));
    font_hygieneMess.loadFromCurrentSurface(renderer);

}

sdlJeu::~sdlJeu () {
    TTF_CloseFont(font);
    TTF_Quit();
    SDL_DestroyRenderer(renderer);
    SDL_DestroyWindow(window);
    SDL_Quit();
}


void sdlJeu::sdlAffichage()
{
    int x, y;
    const Grille& ter = jeu.getConstTerrain();
    const Personnage& perso = jeu.getConstPerso();
    
    //Remplissage écran
    SDL_SetRenderDrawColor(renderer, 255, 255, 255, 255);
    SDL_RenderClear(renderer);
    
    im_sol.draw(renderer,4*zoom,4*zoom,7*zoom,7*zoom);
    im_sol.draw(renderer,4*zoom,11*zoom,7*zoom,7*zoom);
    im_sol.draw(renderer,11*zoom,4*zoom,7*zoom,7*zoom);
    im_sol.draw(renderer,17*zoom,4*zoom,7*zoom,7*zoom);
    im_sol.draw(renderer,11*zoom,11*zoom,7*zoom,7*zoom);
    im_sol.draw(renderer,17*zoom,11*zoom,7*zoom,7*zoom);
    im_sol.draw(renderer,11*zoom,17*zoom,7*zoom,7*zoom);
    im_sol.draw(renderer,17*zoom,17*zoom,7*zoom,7*zoom);
    im_sol.draw(renderer,11*zoom,24*zoom,7*zoom,7*zoom);
    im_sol.draw(renderer,4*zoom,21*zoom,7*zoom,7*zoom);
    
    //Affichage grille
    
    // Affichage du sol de la cuisine (texture x2)
    im_cuisine.draw(renderer,5*zoom,19*zoom,2*zoom,2*zoom);
    im_cuisine.draw(renderer,7*zoom,19*zoom,2*zoom,2*zoom);
    im_cuisine.draw(renderer,9*zoom,19*zoom,2*zoom,2*zoom);
    im_table.draw(renderer,8*zoom,22*zoom,2*zoom,2*zoom);//table cuisine 
    im_table.draw(renderer,5*zoom,19*zoom,2*zoom,zoom);//meubles cuisine
    im_table.draw(renderer,9*zoom,19*zoom,2*zoom,zoom);
    im_table.draw(renderer,5*zoom,11*zoom,5*zoom,zoom);//bibliothèqques 
    im_table.draw(renderer,5*zoom,13*zoom,5*zoom,zoom);
    im_table.draw(renderer,5*zoom,12*zoom,zoom,zoom);
    

    im_sdb.draw(renderer, 5*zoom, 15*zoom, 3*zoom, 3*zoom);
    im_sdb.draw(renderer, 8*zoom, 15*zoom, 3*zoom, 3*zoom);

    im_litCH1.draw(renderer,160,160,100,100); //lit chambre 1
    im_table.draw(renderer,10*zoom,9*zoom,64,32);//bureau chambre 1
    im_ordi.draw(renderer,11*zoom,9*zoom,zoom,zoom);
    im_table.draw(renderer,22*zoom,23*zoom,64,32);//bureai salon
    im_ordi.draw(renderer,23*zoom,23*zoom,zoom,zoom);//ordi salon
    im_litCH2.draw(renderer,17*zoom,7*zoom,4*zoom,3*zoom);//lit chambre 2
    im_table.draw(renderer,22*zoom,5*zoom,2*zoom,zoom);//bureau chambre 2
    im_litCH3.draw(renderer,17*zoom,11*zoom,3*zoom,3*zoom);
    
    im_chaise.draw(renderer,11*zoom,8*zoom,zoom,zoom);//chaise chambre 1
    im_chaise.draw(renderer,23*zoom,22*zoom,zoom,zoom);//chaise chambre 1
    im_tabouret.draw(renderer,8*zoom,21*zoom,zoom,zoom);
    im_tabouret.draw(renderer,9*zoom,21*zoom,zoom,zoom);
    im_tabouret.draw(renderer,7*zoom,23*zoom,zoom,zoom);
    im_tabouret.draw(renderer,23*zoom,6*zoom,zoom,zoom);
    im_evier.draw(renderer,7*zoom,19*zoom,2*zoom,zoom);
    im_canape.draw(renderer,21*zoom,17*zoom,3*zoom,3*zoom);
    im_tv.draw(renderer,20*zoom,18*zoom,0.5*zoom,3*zoom);
    im_plante.draw(renderer,11*zoom,17*zoom,zoom,zoom);
    im_plante.draw(renderer,19*zoom,17*zoom,zoom,zoom);
    im_lavabo2.draw(renderer,5*zoom,17*zoom,zoom,zoom);
    im_baignoire.draw(renderer,8*zoom,16.5*zoom,2*zoom,1.5*zoom);
    im_toilette.draw(renderer,5*zoom,15*zoom,zoom,zoom);
    im_lavabo1.draw(renderer,21*zoom,11*zoom,zoom,zoom);
    im_douche.draw(renderer,23*zoom,11*zoom,zoom,zoom);
    im_tapis.draw(renderer,6*zoom,12*zoom,5*zoom,zoom);

    for (x = 0; x < ter.getX(); x++)
    {
        for (y = 0; y < ter.getY(); y++)
        {
            if(ter.getGrille(x,y) == 'N')
            {
                im_repas.draw(renderer,x*zoom, y*zoom, 0.7*zoom, 0.7*zoom);
            }

            if(ter.getGrille(x,y)=='B')
            {
                im_livre1.draw(renderer,x*zoom,y*zoom,zoom,zoom);
            }
            if(x%4==0)
            {
                im_herbe.draw(renderer,0,x*zoom,4*zoom,4*zoom);
                im_herbe.draw(renderer,25*zoom,x*zoom,5*zoom,4*zoom);
            }
            if(y%4==0)
            {
                im_herbe.draw(renderer,y*zoom,0,4*zoom,4*zoom);
                im_herbe.draw(renderer,y*zoom,25*zoom,4*zoom,5*zoom);
            }

        }
    }
            im_frigo.draw(renderer, 11*zoom, 19*zoom, zoom, zoom);
            im_gaziniere.draw(renderer,9*zoom,19*zoom,zoom,zoom);
        for (x = 0; x < ter.getX(); x++)
        for (y = 0; y < ter.getY(); y++)
        {
            if (ter.getGrille(x,y) == '#') //mur
                {
                    im_mur.draw(renderer, x*zoom, y*zoom, zoom, zoom);
                }
                        // Affichage de la case "soin"
            if (ter.getGrille(x,y) == 'M')
            {
                im_soin.draw(renderer, x*zoom, y*zoom, zoom, zoom);
            }
        }
        



    // Affichage des sprites

    if (perso.direction == "haut")
        im_persoH.draw(renderer,perso.getX()*zoom,perso.getY()*zoom,zoom,zoom);
    
    if (perso.direction == "droite")
        im_persoD.draw(renderer,perso.getX()*zoom,perso.getY()*zoom,zoom,zoom);

    if (perso.direction == "bas")
        im_persoB.draw(renderer,perso.getX()*zoom,perso.getY()*zoom,zoom,zoom);

    if (perso.direction == "gauche")
        im_persoG.draw(renderer,perso.getX()*zoom,perso.getY()*zoom,zoom,zoom);
	




    im_chat.draw(renderer,jeu.chat.getXChat()*zoom, jeu.chat.getYChat()*zoom, zoom, zoom);

    int i = jeu.perso.getFaim();
    std::string tmp = std::to_string(i);
    char const *num_char = tmp.c_str();
    font_faim.setSurface(TTF_RenderText_Solid(font,num_char,font_color));
    font_faim.loadFromCurrentSurface(renderer);

    i = jeu.perso.getSoif();
    tmp = std::to_string(i);
    num_char = tmp.c_str();
    font_soif.setSurface(TTF_RenderText_Solid(font,num_char,font_color));
    font_soif.loadFromCurrentSurface(renderer);

    i = jeu.perso.getSommeil();
    tmp = std::to_string(i);
    num_char = tmp.c_str();
    font_sommeil.setSurface(TTF_RenderText_Solid(font,num_char,font_color));
    font_sommeil.loadFromCurrentSurface(renderer);

    i = jeu.perso.getMental();
    tmp = std::to_string(i);
    num_char = tmp.c_str();
    font_mental.setSurface(TTF_RenderText_Solid(font,num_char,font_color));
    font_mental.loadFromCurrentSurface(renderer);

    i = jeu.perso.getHygiene();
    tmp = std::to_string(i);
    num_char = tmp.c_str();
    font_hygiene.setSurface(TTF_RenderText_Solid(font,num_char,font_color));
    font_hygiene.loadFromCurrentSurface(renderer);

    i = jeu.perso.getSante();
    tmp = std::to_string(i);
    num_char = tmp.c_str();
    font_sante.setSurface(TTF_RenderText_Solid(font,num_char,font_color));
    font_sante.loadFromCurrentSurface(renderer);

    SDL_Rect positionTitre;
    positionTitre.w = 60;
    positionTitre.h = 30;

    positionTitre.x = 5;

    positionTitre.y = 770;
    SDL_RenderCopy(renderer,font_txtFaim.getTexture(),NULL,&positionTitre);

    positionTitre.y = 800;
    SDL_RenderCopy(renderer,font_txtSoif.getTexture(),NULL,&positionTitre);

    positionTitre.w = 90;
    positionTitre.y = 920;
    SDL_RenderCopy(renderer,font_txtSante.getTexture(),NULL,&positionTitre);

    positionTitre.w = 100;
    positionTitre.y = 830;
    SDL_RenderCopy(renderer,font_txtSommeil.getTexture(),NULL,&positionTitre);

    positionTitre.y = 860;
    SDL_RenderCopy(renderer,font_txtMental.getTexture(),NULL,&positionTitre);

    positionTitre.y = 890;
    SDL_RenderCopy(renderer,font_txtHygiene.getTexture(),NULL,&positionTitre);

    positionTitre.y = 770;
    positionTitre.w = 30;
    positionTitre.h = 30;
    positionTitre.x = 70;
    SDL_RenderCopy(renderer,font_faim.getTexture(),NULL,&positionTitre);

    positionTitre.y = 800;
    SDL_RenderCopy(renderer,font_soif.getTexture(),NULL,&positionTitre);

    positionTitre.x = 110;
    positionTitre.y = 830;
    SDL_RenderCopy(renderer,font_sommeil.getTexture(),NULL,&positionTitre);

    positionTitre.y = 860;
    SDL_RenderCopy(renderer,font_mental.getTexture(),NULL,&positionTitre);

    positionTitre.y = 890;
    SDL_RenderCopy(renderer,font_hygiene.getTexture(),NULL,&positionTitre);

    positionTitre.x = 100;
    positionTitre.y = 920;
    SDL_RenderCopy(renderer,font_sante.getTexture(),NULL,&positionTitre);

    positionTitre.w = 500;
    positionTitre.h = 30;
    positionTitre.x = 250;
    positionTitre.y = 50;

    
}


void sdlJeu::sdlBoucle()
{
    SDL_Event events;

    jeu.perso.testRegressionPerso();

    bool ctrl = false;
    bool quit = false;

    int temps = clock(), vitesse = 150;


	// tant que ce n'est pas la fin ...
	
        

        while (!quit)
        {
            

            temps = ((int)clock()/1000);

            jeu.EcouleSommeil(temps, vitesse);
		    jeu.EcouleSoif(temps, vitesse);
    	    jeu.EcouleFaim(temps, vitesse);
    	    jeu.EcouleHygiene(temps, vitesse);
    	    jeu.EcouleSante(temps, vitesse);
		    jeu.EcouleMental(temps, vitesse);
            jeu.DeplacerChat(temps,100);
            jeu.Maladie(temps, 100, vitesse);
            jeu.seBlesse();
            //std::cout<<jeu.perso.getX()<<' '<<jeu.perso.getY()<<std::endl;
            //jeu.perso.afficherPos();
                    if(ctrl)
                    {
                        if(temps%100==0)
                        {
                            jeu.EtatSuivAuto('k');
                        }
                        while (SDL_PollEvent(&events)) {
                        if (events.type == SDL_KEYDOWN) {
                            switch (events.key.keysym.scancode) 
                            {
                                case SDL_SCANCODE_L:
                                    ctrl = false;
                                    std::cout << "SDL_SCANCODE_L" << std::endl;
                                    break;
                                default: break;
                            }
                        }
                    }

                    }
            while (SDL_PollEvent(&events)) 
            {
                if (events.type == SDL_QUIT) quit = true;           // Si l'utilisateur a clique sur la croix de fermeture
                
                    else if (events.type == SDL_KEYDOWN)  
                    {
                        switch (events.key.keysym.scancode) 
                        {
                            case SDL_SCANCODE_LEFT:
                                jeu.EtatSuivCtrl('q');
                            break;
                            case SDL_SCANCODE_RIGHT:
                                jeu.EtatSuivCtrl('d');
                            break;
                            case SDL_SCANCODE_UP:
                                jeu.EtatSuivCtrl('s');
                            break;
                            case SDL_SCANCODE_DOWN:
                                jeu.EtatSuivCtrl('z');
                            break;

                            case SDL_SCANCODE_E:
                                jeu.EtatSuivCtrl('e');
                                
                            break;

                            case SDL_SCANCODE_C:
                                jeu.EtatSuivCtrl('c');
                            break;
                            case SDL_SCANCODE_K:
                                ctrl = true;
                                std::cout << "SDL_SCANCODE_L" << std::endl;
                            break;
                            default: break;
                        }
                    
                    }         
                   } 
                   if (jeu.Mort()) {
                       jeu.FinJeu();

                       quit=true;

                
            }

            sdlAffichage();
    
            SDL_RenderPresent(renderer);

        } 
    
}
