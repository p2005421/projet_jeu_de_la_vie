Projet_jeu_de_la_vie
Informations :

    Auteurs : 
        BOUDEFAR Jounaid p1909916
        MARTINEZ ZOE p2005421
        LE ALEXANDRE p1802486

    Identifiant du projet sur la forge : 23426

    Remerciements :
        DESSEREE Elodie (Rapporteur de projet)
    
Manuel d'utilisation :

Règles : 
    Le but est de survivre pour cela il faut que faut barre de faim,
    soif,santé mentale,hygiène mais surtout de santé ne tombent pas a zero.
    Si votre vie atteint zero vous mourrez.
    Il y a différentes cases sur la carte vous permettant de restaurer
    certaines capacité afin de survivre.

État cellule pour la grille  

Le personnage peut se déplacer sur cette case :

    - '_' --> Sol : le joueur peut se déplacer dessus 
    - 'R' --> Repos : le personnage se repose 
    - 'C' --> Cabine de douche : le personnage se lave, son hygiène remonte
    - 'T' --> Toilettes


Le personnage ne peut pas se déplacer sur cette case :

    - '#' --> Mur
    - 'N' --> Nouriture : le personnage se restaure 
    - 'E' --> Bureau (École) : le personnage se diverti 
    - 'D' --> Divertissement : le personnage se diverti, sa santé mentale augmente 
    - 'L' --> Lavabo : le personnage lave ses mains, son hygiène remonte
    - 'c' --> chat : le personnage peut caresser son chat
    - 'M' --> Kit de soin : Restaure une partie de la santé du personnage

Touches : 
    Version SDL2 : 
        Fleches directionnelles : se déplacer
        'E' : interagir
        'C' : Construit un bloc devant le personnage ou en détruit un
    
    Version TXT : 
        Z Q S D : se déplacer
        'E' : interagir
        'C' : Construire
        'X' : Construction avec coordonnées
        'K' : Mouvement Auto
        'L' : Mouvement Manuel


Makefile :
 make pour compiler la version sdl et ./bin/sd pour l'éxecuter
 make txt : compiler la version txt et ./bin/m pour l'éxecuter
 

 


